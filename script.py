import pandas as pd
import pymongo
data = pd.read_csv("./netflix_titles.csv")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["netflix"]
mycol = mydb["netflixapp"]
c=0
z=1
for row in data.iterrows():
    c+=1
    mydict = {"ref": c,"show_id": row[1]['show_id'],"title":row[1]['title'],"director":row[1]['director'],"cast":row[1]['cast'],"country":row[1]['country'],"date_added":row[1]['date_added'],"release_year":row[1]['release_year'],"rating":row[1]['rating'],"duration":row[1]['duration'],"listed_in":row[1]['listed_in'],"description":row[1]['description'],"type":row[1]['type'],"link":row[1]['link'],"pageno": str(z)}
    x = mycol.insert_one(mydict)
    if(c%20==0):
        z+=1
    